#! /usr/bin/env python3
#
# Written by rupe 02.09.2014, updated 01.23.2022
#
# Pings for active host on *this* sub network.
# The current sub network address and
# the host numbers 1-254 will be pinged
#

from datetime import datetime
from subprocess import getoutput
from threading import Thread, Event
from queue import Queue
from time import localtime
import sys


class PingSweep(Thread):

    def __init__(self, host_num):
        super(self.__class__, self).__init__()
        self.host_num = host_num
        self.hostaddr = None
        self.status = 0
        self._stop_ping_sweep = Event()
        self.information = ("\033[91m" + " No response" + "\033[0m",
                            "\033[92m" + " Host Active" + "\033[0m")

    def run(self):
        """thread pinger function"""
        hostaddr = getoutput('hostname -I').split()[0].split('.')[:-1]
        hostaddr = str('.').join(hostaddr) + '.' + repr(self.host_num)
        line = getoutput("ping -n -i.2 -c 1 %s 2> /dev/null" % hostaddr)

        while not self._stop_ping_sweep.isSet():
            if line.find(
                    hostaddr) and line.find("bytes from") > -1:  # Host Active
                self.status = 1
                self.hostaddr = hostaddr
                break
            elif line.find(hostaddr) and line.find(
                    "Unreachable") > -1:  # No response from host
                self.status = 0
                self.hostaddr = hostaddr
                break
            else:
                self._stop_ping_sweep.set()

    def getStatus(self):
        return self.information[self.status]


if __name__ == '__main__':
    print("""
+-+-+-+-+ +-+-+-+-+ +-+-+-+-+-+
|f|a|s|t| |p|i|n|g| |s|w|e|e|p|
+-+-+-+-+ +-+-+-+-+ +-+-+-+-+-+
 """)
    print("{0}"
          " {1}".format("|  Time  | IP address |", " Results |"))
    print('-+' + "-+-+" * 8 + '-')
    try:
        q = Queue()
        results = list()
        host_nums = [x for x in range(1, 255)]
        [q.put(host_num) for host_num in host_nums]
        while not q.empty():
            for host_num in host_nums:
                t = PingSweep(q.get())
                results.append(t)
        [t.start() for pig in results if (t := pig)]

        t1 = datetime.now()
        for pig in results:
            pig.join()
            if pig.hostaddr is not None:
                print(
                    "\033[96m" + '[%02i:%02i:%02i]\033[0m ' %
                    ((h := localtime())[3], (m := localtime())[4],
                     (s := localtime())[5]), pig.hostaddr, pig.getStatus())
        print('\nPing sweep Completed in:', datetime.now() - t1)
    except KeyboardInterrupt:
        sys.tracebacklimit = 0
        for pig in results:
            if pig is not None:
                pig._stop_ping_sweep.set()
                pig._stop_ping_sweep.wait()
            else:
                pig.join()