#! /usr/bin/env python2
# coding: utf-8# -*- coding: utf-8 -*-
# Port scanner, port 1 to 65535
# By Rupe 12-17-2019

from __future__ import print_function
import socket
from commands import getoutput
from datetime import datetime
from multiprocessing import cpu_count
from multiprocessing.dummy import Pool as ThreadPool
from subprocess import call
from time import asctime


def connect_to_port(port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex((remoteServerIP, port))
    if result is 0:
        print("Port {0}     {1}".format(port, "\033[92mOpen\033[0m"))
    sock.close()


def connectParallel(ports, threads=cpu_count()):
    pool = ThreadPool(threads)
    pool.map_async(connect_to_port, ports)
    pool.close()
    pool.join()


if __name__ == "__main__":
    call(['clear'], shell=True)
    print('\n')
    try:
        remoteServer = raw_input("Enter remote host to scan: ")
        try:
            socket.inet_aton(remoteServer)
        except socket.error:
            exit("[!] Enter a valid IP address!")

        remoteServerIP = socket.gethostbyname(remoteServer)
        host_up = getoutput("sudo ping -n -c1 %s 2> /dev/null" % remoteServerIP)
        if host_up:
            print("-" * 62)
            print("Remote host %s is Up @ %s" % (remoteServerIP, asctime()))
        print("#" * 62)
        print(
            "Please wait, scanning 65535 ports on remote host", remoteServerIP
        )
        print("#" * 62)
        ports = [x for x in range(1, 65536)]
        t1 = datetime.now()
        connectParallel(ports)
    except KeyboardInterrupt:
        exit("\n\033[91m[" + u'\u2718' + "]\033[0m User Requested to Abort!")
    print('Scanning Completed in:', datetime.now() - t1)