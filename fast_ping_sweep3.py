#! /usr/bin/env python3
#
# Written by rupe 02.09.2014, updated 01.23.2022
#
# Pings for active host
# The current list of host address
#

import sys
from subprocess import getoutput
from datetime import datetime
from time import localtime
from os import geteuid
from queue import Queue
from threading import Event, Thread


class PingSweep(Thread):

    def __init__(self, host_addr):
        super(self.__class__, self).__init__()
        self.host_addr = host_addr
        self.status = 0
        self._stop_ping_sweep = Event()
        self.information = ("\033[91m" + " No response" + "\033[0m",
                            "\033[92m" + " Host Active" + "\033[0m")

    def run(self):
        """thread pinger function"""
        line = getoutput("ping -n -i.2 -c 1 %s 2> /dev/null" % self.host_addr)

        while not self._stop_ping_sweep.isSet():
            if line.find(self.host_addr
                        ) and line.find("bytes from") > -1:  # Host Active
                self.status = 1
                break
            elif line.find(self.host_addr) and line.find(
                    "Unreachable") > -1:  # No response from host
                self.status = 0
                break
            else:
                self._stop_ping_sweep.set()

    def getStatus(self):
        return self.information[self.status]


if __name__ == '__main__':
    print("""
+-+-+-+-+ +-+-+-+-+ +-+-+-+-+-+
|f|a|s|t| |p|i|n|g| |s|w|e|e|p|
+-+-+-+-+ +-+-+-+-+ +-+-+-+-+-+
 """)
    print("{0}"
          " {1}".format("|  Time  | IP address |", " Results |"))
    print('-+' + "-+-+" * 8 + '-')
    try:
        with open('iplist.txt') as f:
            host_addrs = f.read().splitlines()
    except IOError:
        exit("\n\033[1;31m[!] File iplist.txt missing!\033[0m")

    try:
        q = Queue()
        results = list()
        [q.put(host_addr) for que in host_addrs if (host_addr := que)]
        while not q.empty():
            t = PingSweep(q.get())
            results.append(t)
        [t.start() for pig in results if (t := pig)]

        t1 = datetime.now()
        for pig in results:
            pig.join()
            if pig.host_addr is not None:
                print(
                    "\033[96m" + '[%02i:%02i:%02i]\033[0m ' %
                    ((h := localtime())[3], (m := localtime())[4],
                     (s := localtime())[5]), pig.host_addr, pig.getStatus())
        print('\nPing sweep Completed in:', datetime.now() - t1)
    except KeyboardInterrupt:
        sys.tracebacklimit = 0
        for pig in results:
            if pig is not None:
                pig._stop_ping_sweep.set()
                pig._stop_ping_sweep.wait()
            else:
                pig.join()