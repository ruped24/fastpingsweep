![](https://img.shields.io/badge/fastpingsweep-python_2.7-blue.svg?style=flat-square)

```
#!text

Pings for active host on *this* sub network. 
The current sub network address and the host numbers 1-254 will be pinged.
```